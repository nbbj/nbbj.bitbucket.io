var scene, camera, renderer, outline;
var geometry, bufferG, material, mesh;
var time = new Date();

window.document.body.addEventListener('load', init());
animate();

function init() {
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0.92,0.92,0.92);
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.up = new THREE.Vector3(0,0,1);

    //load geoometry
    material = new THREE.MeshBasicMaterial( { color: 'white', wireframe: false } );
    var loader = new THREE.OBJLoader();
    loader.crossOrigin = '';
    loader.load( '3.obj', function(event){
        console.log(event);
        event.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
                child.material = material;
                var edges = new THREE.EdgesGeometry( child.geometry );
               var col = new THREE.Color(child.name);
                var lines = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: col, linewidth: 5 } ) );
                scene.add(lines );
                var line = new MeshLine();
                line.setGeometry( child.geometry );
                //TODO: replace with meshlines
                //console.log(line); 
                //var meshMat = new MeshLineMaterial({ color: col, linewidth: 3 });
                //var mesh = new THREE.Mesh(line.geometry, material);
                //scene.add( mesh );
                if (child.name == 'black') si = THREE.DoubleSide; else si = THREE.BackSide
                var outlineMaterial = new THREE.MeshBasicMaterial( { color: 'black', side: si } );
                var outlineMesh = new THREE.Mesh( child.geometry, outlineMaterial );
                var bbox = new THREE.Box3().setFromObject(outlineMesh);
                outlineMesh.position = bbox.getCenter();
	            outlineMesh.scale.multiplyScalar(1.006);
	            scene.add( outlineMesh );
            }
        });  
        scene.add(event);
        
    });

	renderer = new THREE.WebGLRenderer();
	renderer.setSize( window.innerWidth, window.innerHeight );
  	document.body.appendChild( renderer.domElement );
}

function animate() {
        requestAnimationFrame( animate );
        time = new Date(time.getTime() + 1*60000);
        var sunPos = SunCalc.getPosition(time, 47.6, -122.3);
        var zoom = 200;
        camera.position.x = zoom*Math.sin(sunPos.azimuth)*Math.cos(sunPos.altitude);
        camera.position.y = zoom*Math.cos(sunPos.azimuth)*Math.cos(sunPos.altitude);
        camera.position.z = zoom*Math.sin(sunPos.altitude);
        camera.lookAt(new THREE.Vector3(0,0,0));
        renderer.render( scene, camera ); 
}